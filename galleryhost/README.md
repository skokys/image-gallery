To build dist package

    gradle distZip
    
To run

    java -DWEB=web -DLIVE_URL=http://localhost:6789 -DPORT=8080 -jar galleryhost.jar
    
Where WEB is web directory, LIVE_URL is url to motion server and PORT is port to listen on.

Logging setting https://www.slf4j.org/api/org/slf4j/impl/SimpleLogger.html


