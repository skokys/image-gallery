package skoky

import com.google.gson.Gson
import org.eclipse.jetty.io.EofException
import org.slf4j.LoggerFactory
import spark.Response
import spark.Spark.*
import java.io.File
import java.io.FileFilter
import java.io.FileInputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.imageio.ImageIO

object Main {

    private val LIVE_URL = "LIVE_URL"   // -D LIVE_URL=http://localhost:6789
    private val WEB_DIR_P = "WEB"       // -D WEB=web
    private val PORT_P = "PORT"         // default 80
    private val MAX_AGE = 86400
    private val log: org.slf4j.Logger = LoggerFactory.getLogger(Main::class.java)
    // run with debug -Dorg.slf4j.simpleLogger.defaultLogLevel=debug

    @JvmStatic
    fun main(args: Array<String>) {
        when (System.getProperty(PORT_P).orEmpty()) {
            "" -> port(80)
            else -> try {
                port(Integer.parseInt(System.getProperty(PORT_P)))
            } catch (e: Exception) {
                System.err.println("Invalid port")
            }
        }

        val liveUrl = when (System.getProperty(LIVE_URL).orEmpty()) {
            "" -> "http://localhost:6789"
            else -> System.getProperty(LIVE_URL)
        }

        when (System.getProperty(WEB_DIR_P).orEmpty()) {
            "" -> externalStaticFileLocation("web")
            else -> externalStaticFileLocation(System.getProperty(WEB_DIR_P))
        }
        get("/live") { request, response ->
            log.debug(Thread.currentThread().name + ":Connected client -> ${request.ip()}/${request.userAgent()}")
            response.status(200)
            response.header("Cache-Control", "no-cache, private")
            response.type("multipart/x-mixed-replace; boundary=--BoundaryString")
            response.header("Connection", "close")
            response.header("Expires", "0")
            response.header("Max-Age", "0")
            response.header("Pragma", "no-cache")

            val con = URL(liveUrl).openConnection() as HttpURLConnection
            pushBuf(con.inputStream, response)
        }

        get("/images") { _, response ->
            response.status(200)
            response.header("Cache-Control", "public, max-age=60")
            Gson().toJson(getSortedFileList(FileFilter { it.name.endsWith(".jpg") }))
        }

        get("/videos") { _, response ->
            response.status(200)
            response.header("Cache-Control", "public, max-age=60")
            Gson().toJson(getSortedFileList(FileFilter { it.name.endsWith(".swf") }))
        }

        /// date limit in milliseconds
        get("/images/:limit") { request, response ->
            response.status(200)
            response.header("Cache-Control", "public, max-age=10")
            val limit = request.params(":limit").toLongOrNull() ?: Long.MAX_VALUE
            val oldest = System.currentTimeMillis() - limit

            val files = File(Imager.DIR).listFiles(FileFilter {
                it.name.endsWith(".jpg") && it.lastModified() > oldest
            })
            files.sortByDescending { it.lastModified() }

            Gson().toJson(files.map { it.name })
        }

        get("/image/:name") { request, response ->

            val name = request.params(":name")
            val img = Imager.fromFile(name)
            if (img == null) {
                log.warn("Image not found: $name")
                response.status(404)
                response.body("image not found $name")
            } else {
                ImageIO.write(img, "jpg", response.raw().outputStream)
                response.status(200)
                response.header("Cache-Control", "public, max-age=$MAX_AGE")
            }
        }

        get("/imagethum/:name") { request, response ->

            val imageName = request.params(":name")

            if (!File("${Imager.DIR}/$imageName").exists()) {
                log.warn("Image not found: $imageName")
                response.status(404)
                response.body("image ot found $imageName")
            } else {

                val thumb = if (!File("${Imager.DIR_THUMB}/$imageName").exists()) {
                    Imager.scaleAndStore(imageName)
                } else {
                    Imager.getThumb(imageName)
                }

                if (thumb == null) {
                    log.warn("Unable to scale image $imageName")
                    response.status(500)
                } else {
                    ImageIO.write(thumb, "jpg", response.raw().outputStream)
                    response.header("Cache-Control", "public, max-age=$MAX_AGE")
                    response.status(200)
                }
            }

        }

        get("/video/:name") { request, response ->
            val name = request.params(":name")
            val file = File("${Imager.DIR}/$name")
            if (file.exists()) {
                response.header("Content-Type", "application/x-shockwave-flash")
                pushBuf(FileInputStream(file), response)
                response.status(200)
                response.header("Cache-Control", "public, max-age=$MAX_AGE")
            } else {
                response.status(404)
            }

        }
        println("Setup done. Listening on port ${port()}, live url $liveUrl")
    }

    private fun pushBuf(fis: InputStream, response: Response) {
        val buf = ByteArray(2048)
        while (true)
            try {
                val read = fis.read(buf)
                if (read <= 0) break
                response.raw().outputStream.write(buf)
            } catch (e: EofException) {
                log.debug("Disconnected client ${e.message}")
                break
            } catch (ex: Exception) {
                log.debug("Disconnected client with error ${ex.message}")
                break
            }
    }

    fun getSortedFileList(filter: FileFilter): Array<String> =
            File(Imager.DIR).listFiles(filter).filter { it.length() > 0 }.map { it.name }.sortedDescending().toTypedArray()

}
