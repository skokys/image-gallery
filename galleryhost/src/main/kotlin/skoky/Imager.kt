package skoky

import khttp.post
import org.slf4j.LoggerFactory
import java.awt.image.BufferedImage
import java.io.File
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.PosixFilePermissions
import javax.imageio.ImageIO

/**
 * Created by skoky on 21.2.15.
 */
class Imager {

    private class WatchingThread(private val watchService: WatchService) : Thread() {

        override fun run() {
            super.run()
            try {
                var counter = 0
                while (true) {
                    val watchKey = watchService.take()
                    val watchEvents = watchKey.pollEvents()
                    for (we in watchEvents) {
                        if (we.kind() === StandardWatchEventKinds.ENTRY_CREATE) {
                            val filename = we.context().toString()
                            log.debug("New image to be resized: " + filename)
                            if (filename.endsWith(".jpg")) {
                                Imager.scaleAndStore(filename)
                            }
                            counter++
                            if (counter > 100) {
                                deleteOldThumbs()
                                counter = 0
                            }
                            if (counter % 10 == 0) {
                                upload(filename)
                            }
                        }

                    }
                    if (!watchKey.reset()) {
                        break
                    }
                }
            } catch (e: InterruptedException) {
                log.error("Interrupted")
            }

        }

        private fun upload(filename: String) {
            val startTime = System.currentTimeMillis()
            val file = File("$DIR/$filename")
            val r = post("https://gallery-182613.appspot.com/img", data = file)
//            log.info("Upload result $r, ${data.length()}b time ${System.currentTimeMillis()-startTime}ms")
            if (r.statusCode != 200) {
                log.warn("Upload result $r, ${file.length()}b time ${System.currentTimeMillis() - startTime}ms")
            }
        }

        private fun deleteOldThumbs() {

            try {
                val files = Files.list(Paths.get(DIR))
                files!!.forEach { f ->
                    try {
                        if (f.fileName.toString().endsWith(".swf"))
                            Files.delete(f)
                        else if (f.fileName.toString().endsWith(".jpg")
                                && File(f.toUri()).lastModified() < System.currentTimeMillis() - ONE_DAY_MS) {
                            Files.delete(f)
                        }

                    } catch (e: IOException) {
                        log.warn("Unable to delete file:" + f)
                    }
                }

                val filesThumb = Files.list(Paths.get(DIR_THUMB))
                filesThumb!!.forEach { f ->
                    try {
                        if (f.fileName.toString().endsWith(".jpg")
                                && File(f.toUri()).lastModified() < System.currentTimeMillis() - ONE_DAY_MS) {
                            Files.delete(f)
                        }

                    } catch (e: IOException) {
                        log.warn("Unable to delete file:" + f)
                    }
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    companion object {

        val DIR_THUMB = "/opt/motion/thumbs"
        val DIR = "/opt/motion"
        val ONE_DAY_MS: Long = 86400000
        internal val log: org.slf4j.Logger = LoggerFactory.getLogger(Imager::class.java)

        init {

            if (!File(DIR_THUMB).exists()) {        // create thumb dir if does not exists
                val attr = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxrwxrwx"))
                try {
                    Files.createDirectory(Paths.get(DIR_THUMB), attr)
                } catch (e: IOException) {
                    log.error("Can't setup dir $DIR_THUMB e: $e")
                }

            }

            val p = Paths.get(DIR)
            val fileSystem = FileSystems.getDefault()
            try {
                val watchService = fileSystem.newWatchService()
                p.register(watchService, StandardWatchEventKinds.ENTRY_CREATE)
                WatchingThread(watchService).start()
            } catch (e: IOException) {
                log.error("Can't setup dir $DIR e: $e")
            }
        }

        private fun scale(source: BufferedImage?, ratio: Double): BufferedImage? {
            source?.let {
                val w = (it.width * ratio).toInt()
                val h = (it.height * ratio).toInt()
                val start = System.currentTimeMillis()
                val scaled = ImageUtils.resizeImage(source, ImageUtils.IMAGE_JPEG, w, h)
                log.debug("Scaled size $scaled in ${System.currentTimeMillis()-start}ms")
                return scaled
            }
            log.warn("No image to scale")
            return null
        }

        fun getThumb(imageName: String): BufferedImage? {
            return try {
                ImageIO.read(File("$DIR_THUMB/$imageName"))
            } catch (e: IOException) {
//                log.warn("Can open file $DIR_THUMB/$imageName")
                null
            }

        }

        fun scaleAndStore(imageName: String): BufferedImage? {

            val scaled = scale(fromFile(imageName), 0.1)

            scaled?.let {
                val of = File("$DIR_THUMB/$imageName")
                try {
                    val written = ImageIO.write(scaled, "jpg", of)
                    log.debug("Written image thumb $written")
                } catch (e: IOException) {
                    log.warn("Thumb write failed: ${of.absolutePath} e: $e")
                }
                return scaled
            }
            log.warn("No image to store")
            return null
        }

        fun fromFile(imageName: String): BufferedImage? {

            val file = File("$DIR/$imageName")
            val img = try {
                if (!file.exists()) {
                    log.warn("File does not exists ${file.absoluteFile}")
                    null
                } else
                    ImageIO.read(file)
            } catch (e: Exception) {
                log.warn("Error reading file ${e.message}")
                null
            }
            if (img == null) log.warn("Reading file is null ${file.absoluteFile}")
            return img
        }
    }
}
