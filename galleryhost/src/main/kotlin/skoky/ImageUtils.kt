package skoky

import org.slf4j.LoggerFactory
import java.awt.Dimension
import java.awt.Image
import java.awt.image.BufferedImage
import java.awt.image.PixelGrabber


/**
 * Utilities methods for image manipulation. It does not support writting of GIF images, but it can
 * read from. GIF images will be saved as PNG.
 *
 * @author Rafael Steil
 * @version $Id: ImageUtils.java,v 1.23 2007/09/09 01:05:22 rafaelsteil Exp $
 */
class ImageUtils {
    internal val log: org.slf4j.Logger = LoggerFactory.getLogger(ImageUtils::class.java)

    companion object {
        val IMAGE_JPEG = 0
        val IMAGE_PNG = 1

        /**
         * Resizes an image.
         *
         * @param image
         * The image to resize
         * @param maxWidth
         * The image's max width
         * @param maxHeight
         * The image's max height
         * @return A resized `BufferedImage`
         * @param type
         * int
         */
        fun resizeImage(image: BufferedImage, type: Int, maxWidth: Int, maxHeight: Int): BufferedImage {
            val largestDimension = Dimension(maxWidth, maxHeight)

            // Original size
            var imageWidth = image.getWidth(null)
            var imageHeight = image.getHeight(null)

            val aspectRatio = imageWidth.toFloat() / imageHeight

            if (imageWidth > maxWidth || imageHeight > maxHeight) {
                if (largestDimension.width.toFloat() / largestDimension.height > aspectRatio) {
                    largestDimension.width = Math.ceil((largestDimension.height * aspectRatio).toDouble()).toInt()
                } else {
                    largestDimension.height = Math.ceil((largestDimension.width / aspectRatio).toDouble()).toInt()
                }

                imageWidth = largestDimension.width
                imageHeight = largestDimension.height
            }

            return createHeadlessSmoothBufferedImage(image, type, imageWidth, imageHeight)
        }

        /**
         * Creates a `BufferedImage` from an `Image`. This method can
         * function on a completely headless system. This especially includes Linux and Unix systems
         * that do not have the X11 libraries installed, which are required for the AWT subsystem to
         * operate. The resulting image will be smoothly scaled using bilinear filtering.
         *
         * @param source The image to convert
         * @param w The desired image width
         * @param h The desired image height
         * @return The converted image
         * @param iType  int
         */
        private fun createHeadlessSmoothBufferedImage(source: BufferedImage, iType: Int, width: Int, height: Int): BufferedImage {
            val imgType = if (iType == skoky.ImageUtils.IMAGE_PNG && hasAlpha(source)) {
                BufferedImage.TYPE_INT_ARGB
            } else {
                BufferedImage.TYPE_INT_RGB
            }
            val dest = BufferedImage(width, height, imgType)

            val scalex = width.toDouble() / source.width
            val scaley = height.toDouble() / source.height

            for (y in 0..height - 1) {
                val sourcey = y * source.height / dest.height
                val ydiff = scale(y, scaley) - sourcey

                for (x in 0..width - 1) {
                    val sourcex = x * source.width / dest.width
                    val xdiff = scale(x, scalex) - sourcex

                    val x1 = Math.min(source.width - 1, sourcex + 1)
                    val y1 = Math.min(source.height - 1, sourcey + 1)

                    val rgb1 = getRGBInterpolation(source.getRGB(sourcex, sourcey), source.getRGB(x1, sourcey), xdiff)
                    val rgb2 = getRGBInterpolation(source.getRGB(sourcex, y1), source.getRGB(x1, y1), xdiff)

                    val rgb = getRGBInterpolation(rgb1, rgb2, ydiff)

                    dest.setRGB(x, y, rgb)
                }
            }

            return dest
        }

        private fun scale(point: Int, scale: Double): Double = point / scale

        private fun getRGBInterpolation(value1: Int, value2: Int, distance: Double): Int {
            val alpha1 = (value1 and 0xFF000000.toInt()).ushr(24)
            val red1 = value1 and 0x00FF0000 shr 16
            val green1 = value1 and 0x0000FF00 shr 8
            val blue1 = value1 and 0x000000FF

            val alpha2 = (value2 and 0xFF000000.toInt()).ushr(24)
            val red2 = value2 and 0x00FF0000 shr 16
            val green2 = value2 and 0x0000FF00 shr 8
            val blue2 = value2 and 0x000000FF

            return (alpha1 * (1.0 - distance) + alpha2 * distance).toInt() shl 24 or ((red1 * (1.0 - distance) + red2 * distance).toInt() shl 16) or ((green1 * (1.0 - distance) + green2 * distance).toInt() shl 8) or (blue1 * (1.0 - distance) + blue2 * distance).toInt()
        }

        /**
         * Determines if the image has transparent pixels.
         *
         * @param image The image to check for transparent pixel.s
         * @return `true` of `false`, according to the result
         */
        private fun hasAlpha(image: Image): Boolean {
            return try {
                val pg = PixelGrabber(image, 0, 0, 1, 1, false)
                pg.grabPixels()

                pg.colorModel.hasAlpha()
            } catch (e: InterruptedException) {
                false
            }

        }
    }
}




