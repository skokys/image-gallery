package skoky

import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Test
import skoky.Main.getSortedFileList
import java.io.File
import java.io.FileFilter


class MainTest {

    @Test
    fun testSort() {

        try {
            File(Imager.DIR).mkdirs()
            val b = "Hi".toByteArray()
            File("${Imager.DIR}/file1.txt").writeBytes(b)
            File("${Imager.DIR}/file1.txt").writeBytes(b)
            File("${Imager.DIR}/file1.txt").writeBytes(b)

            val a = getSortedFileList(FileFilter { it.length() >0 })
            val shouldBe = File(Imager.DIR).listFiles()
                    .map{ it.name }.toTypedArray().sortedArray()

            assertArrayEquals("Arrays no match -> ", shouldBe, a)
        } catch (e: Exception) {
            println(">>>>" + e.message)
            throw e

        }
    }

    @Test
    fun checkParams() {

        var port = when (System.getProperty("PORT").orEmpty()) {
            "" -> 80
            else -> try {
                Integer.parseInt(System.getProperty("PORT"))
            } catch (e: Exception) {
                System.err.println("Invalid port")
                -1
            }
        }
        assertEquals("Port does not match",80,port)
    }

    @Test
    fun checkParams2() {

        var port = when ("999") {
            "" -> 80
            else -> try {
                Integer.parseInt("999")
            } catch (e: Exception) {
                System.err.println("Invalid port")
                -1
            }
        }
        assertEquals("Port does not match",999,port)
    }

    @Test
    fun checkParams3() {

        val x = "99xx"
        var port = when (x) {
            "" -> 80
            else -> try {
                Integer.parseInt("999")
            } catch (e: Exception) {
                System.err.println(x)
                -1
            }
        }
        assertEquals("Port does not match",999,port)
    }

}