#!/bin/bash

# install in advance:
# sudo apt-get install imagemagick imagemagick-doc bc nginx
# create initial html to show image.jpg
# nginx config snap:
#        location /thumbs {
#                autoindex on;
#                autoindex_format json;
#
#        }
# RAM disk
# sudo mkdir -p /ram
# sudo mount -t tmpfs -o size=100m tmpfs /ram

IMG=img.jpg
IMGORG=img.org.jpg
IMGCLOUD=img.small.jpg
CLOUD=https://gallery-182613.appspot.com/img

HTMLDIR=/var/www/html
ARCHDIR=$HTMLDIR/arch
mkdir -p $ARCHDIR

THUMBDIR=$HTMLDIR/thumbs
mkdir -p $THUMBDIR
WORKDIR=/ram

while true
do
	date +"%T"> $WORKDIR/last.time.txt
	raspistill -t 1500 -w 768 -h 1024 -rot 270 --annotate "$(date)" -o $WORKDIR/$IMG
	cp "$WORKDIR/$IMG" "$HTMLDIR/$IMG"
    SIZE=(`compare -metric PSNR $WORKDIR/$IMG $WORKDIR/$IMGORG /dev/null 2>&1`)
	C=`echo "$SIZE <35" | bc`
	echo "Score: $SIZE, C: $C."
	if [ $C == 0 ]; then
	    echo "No change $WORKDIR/$IMG, $HTMLDIR/$IMG"
	else
	    IMGARCH=`echo "$IMG-$(date).jpg" | tr ' ' '-'`
   	    echo "New image, copy to cloud $IMGARCH"
   	    (convert "$WORKDIR/$IMG" -resize 10% "$THUMBDIR/$IMGARCH")
	    echo "Uploading img to cloud "
	    (convert "$WORKDIR/$IMG" -resize 50% "$WORKDIR/$IMGCLOUD"; cp "$WORKDIR/$IMGCLOUD" "$ARCHDIR/$IMGARCH"; curl -q -X POST $CLOUD --data-binary "$WORKDIR/$IMGCLOUD" )
	fi

	find $ARCHDIR -mtime +5 -exec rm {} \; &
	find $THUMBDIR -mtime +5 -exec rm {} \; &
	cp $WORKDIR/$IMG $WORKDIR/$IMGORG

done
