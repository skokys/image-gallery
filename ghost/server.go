package server

import (
    "context"
    "fmt"
    "google.golang.org/appengine"
    "google.golang.org/appengine/log"
    "google.golang.org/appengine/memcache"
    "net/http"
    "time"
    "io/ioutil"
    "encoding/binary"
    "strconv"
)

func init() {
    http.HandleFunc("/img", handlerPush)
    http.HandleFunc("/img.jpg", handlerPush)
}

func handlerPush(w http.ResponseWriter, r *http.Request) {

    if r.Method == "POST" {
        post(w, r)
    } else if r.Method == "GET" {
        get(w, r)
    } else {
        w.WriteHeader(405)
        return
    }
}

func get(w http.ResponseWriter, r *http.Request) {
    ctx := appengine.NewContext(r)

    data, err := getKeyFromCache(ctx, "IMAGE")
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte("Unable to read from cache"))
        return
    }

    if len(data) < 4 {
        w.WriteHeader(http.StatusGone)
        w.Write([]byte("Empty cache"))
        return
    }

    img := decode(data)
    log.Infof(ctx, "Image decoded ok")

    since := r.Header.Get("If-Modified-Since")
    if since != "" {
        t, e := time.Parse(http.TimeFormat, since)
        if e != nil {
            w.WriteHeader(http.StatusExpectationFailed)
            w.Write([]byte("Invalid time format in 'If-Modified-Since' value " + since))
            return
        }
        diff := img.getLastUpdate() - t.Unix()
        if diff > 3 { // image not modified
            w.WriteHeader(http.StatusNotModified)
            w.Write([]byte("No new image in cache"))
            return
        }
        w.Header().Set("X-Cached-Time", strconv.FormatInt(diff, 10))
    }

    w.Header().Set("Content-Type", "image/jpeg")
    w.Header().Set("Content-Disposition", "name=\"file\"; filename=\"img.jpg\"")
    w.Header().Set("Last-Modified", time.Unix(img.getLastUpdate(), 0).Format(http.TimeFormat))
    w.Write(img.getImgData())
}

func post(w http.ResponseWriter, r *http.Request) {
    data, err := ioutil.ReadAll(r.Body)
    if err != nil {
        fmt.Fprintf(w, "Error reading bytes: %v", err)
        w.WriteHeader(500)
        return
    }

    ctx := appengine.NewContext(r)
    log.Infof(ctx, "Data size received %d", len(data))
    var img = ImgData{imgData: data, lastUpdate: time.Now().Unix()}
    setKeyToCache(ctx, "IMAGE", encode(img))
    w.WriteHeader(200)
}

func setKeyToCache(ctx context.Context, key string, value []byte) {
    item := &memcache.Item{
        Key:        key,
        Value:      value,
        Expiration: 0,
    }
    if err := memcache.Add(ctx, item); err == memcache.ErrNotStored {
        log.Infof(ctx, "item with key %q already exists", item.Key)
        if err := memcache.Set(ctx, item); err != nil {
            log.Errorf(ctx, "error setting item: %v", err)
        } else {
            log.Infof(ctx, "cache updated")
        }
    }

}

func encode(value Img) []byte {
    b := make([]byte, 8)
    binary.LittleEndian.PutUint64(b, uint64(value.getLastUpdate()))
    return append(b, value.getImgData()...)
}

func decode(data []byte) Img {
    lastUpdate := binary.LittleEndian.Uint64(data)
    imgData := data[8:]
    return ImgData{lastUpdate: int64(lastUpdate), imgData: imgData,}
}

func getKeyFromCache(ctx context.Context, key string) ([]byte, error) {

    if item, err := memcache.Get(ctx, key); err == memcache.ErrCacheMiss {
        log.Infof(ctx, "item not in the cache")
        return []byte{}, nil
    } else if err != nil {
        log.Infof(ctx, "Error %v", err)
        return nil, err
    } else {
        return item.Value, nil
    }
}

type Img interface {
    getImgData() []byte
    getLastUpdate() int64
}

type ImgData struct {
    imgData    []byte
    lastUpdate int64
}

func (i ImgData) getImgData() []byte {
    return i.imgData
}
func (i ImgData) getLastUpdate() int64 {
    return i.lastUpdate
}
