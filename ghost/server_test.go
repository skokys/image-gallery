package server

import (
    "testing"
    "bytes"
)

func TestEncoding(t *testing.T) {

    originalImg := ImgData{lastUpdate:123,imgData:[]byte{0x41,0x42,0x43,0x43,0x43,0x43},}

    encoded := encode(originalImg)
    decoded := decode(encoded)

    if originalImg.lastUpdate != decoded.getLastUpdate() {
        t.Errorf("Last update does not match")
    }

    if bytes.Compare(originalImg.imgData,decoded.getImgData())!=0 {
        t.Errorf("Image data issue")
    }

}