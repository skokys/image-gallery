import 'dart:html';
import 'dart:convert';

main() {

  var url = '/images';
  var url2 = '/imagethum/';
  var url3 = '/image/';

  // call the web server asynchronously
  var request = HttpRequest.getString(url).then((response) {

    var r = response.toString();
    print(r);

    var parsedMap = JSON.decode(r);

    querySelector('#spinner').remove();

    parsedMap.forEach((m){
      var a = new AnchorElement();
      a.href=url3+m.toString();
      ImageElement img = new ImageElement();
      img.alt=m;
      img.src=url2+m.toString();
      img.className="img-thumbnail thumb";
      a.children.add(img);
      querySelector('#images').children.add(a);
    });
  });


}