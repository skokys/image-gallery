import 'dart:html';
import 'dart:convert';

var url = '/images/3600000';
var url2 = '/imagethum/';
var url3 = '/image/';

refreshLastHourThumbs() {
  // call the web server asynchronously
  var request = HttpRequest.getString(url).then((response) {

    var r = response.toString();
    print(r);
    var parsedMap = JSON.decode(r);
    var imagesEl = querySelector('#images');
    imagesEl.children.clear();

    parsedMap.forEach((m){
      var a = new AnchorElement();
      a.href=url3+m.toString();
      ImageElement img = new ImageElement();
      img.alt=m;
      img.src=url2+m.toString();
      img.className="img-thumbnail thumb";
      a.children.add(img);
      imagesEl.children.add(a);
    });
    if (parsedMap.length==0) {
      var a = new DivElement();
      a.className="alert alert-success";
      LabelElement l = new LabelElement();
      l.text = "Žádná aktivita";
      a.children.add(l);
      imagesEl.children.add(a);
    }
  });

}

main() {

//  querySelector('#refresh').on.click.add(refreshLastHourThumbs());

  querySelector('#refresh').onClick.listen((e)=>refreshLastHourThumbs());
  refreshLastHourThumbs();

}