import 'dart:html';
import 'dart:convert';

main() {

  var url = '/videos';
  var url3 = '/video/';

  // call the web server asynchronously
  var request = HttpRequest.getString(url).then((response) {

    var r = response.toString();
    print(r);

    var parsedMap = JSON.decode(r);

    parsedMap.forEach((m){
      var a = new AnchorElement();
      a.href=url3+m.toString();
      LabelElement lbl = new LabelElement();
      lbl.text = m.toString();
      a.children.add(lbl);
      a.children.add(new BRElement());
      querySelector('#videos').children.add(a);
    });
  });


}