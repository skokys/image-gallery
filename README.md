# Image gallery and online camera for Raspberry PI  #

This application connects to USB camera using 'motion' app available on Linux. It shows current live view from the
camera and stores image snapshots if there is a move on in front of the camera. These snapshots with live view
 are shown on a website with last hour changes. There is also archive of all snapshots for one month. 

Its great app for broadcasting from home and Raspberry Pi. Its energy efficient and quite. 

* Version 1.0

The application is written in Java (web server) and Dart (client site). There is [Spark framework](http://sparkjava.com) used as a web server.


## Setup ##

* install motion app to linux like 'sudo apt-get install motion'
* copy motion/motion.conf to /etc/motion/motion.conf. Customize if necessary.
* build client side using Dart tool
* build server-side using Gradle by 'gradle distZip'
* copy build/distributions/Gallery.zip to Raaspberry Pi
* run 'java -jar galleryhost.jar' on server
* open browser on http://raspihost to see website


## Contribution guidelines ##

I'll be happy to integrate pull requests if it makes the app better

